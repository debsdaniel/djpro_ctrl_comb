from django.test import TestCase, SimpleTestCase

class HomePageTests(TestCase):
    databases = {'default'}

    def setUp(self):
        self.response = self.client.get("/")
    
    def test_url_home(self):
        self.assertEqual(self.response.status_code,200)
    
    def test_plantilla_home(self):
        self.assertTemplateUsed(
            self.response,"bases/home.html"
        )
        self.assertContains(
            self.response, "Inicio"
        )
        self.assertNotContains(
            self.response,
            "Curso Daniel Bojorge /FB/IG/DEBSconsultores"
        )
